﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public float hp = 100;
    public float moveSpeed;
    public float turnSpeed;

    public Transform attacker;

    public int targetIndex { get; set; }

// Update is called once per frame
void Update () {
        //RotateToAttacker();
        Move();
        CheckDie();
	}

    void Move()
    {
        transform.position += transform.forward * moveSpeed * Time.deltaTime;
    }

    void RotateToAttacker()
    {
        if (attacker != null)
        {
            Vector3 attackerDirection = attacker.position - transform.position;
            Vector3 newDirection = Vector3.RotateTowards(transform.forward, attackerDirection, turnSpeed * Time.deltaTime, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDirection);
        }
    }

    void CheckDie()
    {
        if (hp <= 0f)
        {
            EnemySpawner.enemyDeath(this.gameObject);
            Destroy(gameObject);
        }
    }
}
