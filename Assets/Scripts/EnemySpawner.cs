﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour {

    public GameObject enemyToSpawn;
    public Vector2 spawnRange;
    public float spawnDelay;
    public Vector2 enemySpeed;
    public Vector2 enemyHealth;

    public float sizeMultiplier;
    public float speedMultiplier;

    public static List<GameObject> enemyList;

    private float spawnTimer;
	
    void Start()
    {
        enemyList = new List<GameObject>();
    }

	void Update () {
        SpawnTimer();
        SpawnEnemy();
	}

    void SpawnTimer()
    {
        spawnTimer += Time.deltaTime;
    }

    void SpawnEnemy()
    {
        if (spawnTimer > spawnDelay)
        {
            Vector3 spawnPosition = new Vector3 (0,0,Random.Range(spawnRange.x, spawnRange.y));
            GameObject enemyInstance = (GameObject)Instantiate(enemyToSpawn, transform.position + spawnPosition, transform.rotation);
            Enemy thisEnemy = enemyInstance.GetComponent<Enemy>();
            enemyList.Add(enemyInstance);
            thisEnemy.targetIndex = enemyList.Count;

            thisEnemy.hp = Random.Range(enemyHealth.x, enemyHealth.y);
            enemyInstance.transform.localScale = enemyInstance.transform.localScale * thisEnemy.hp * sizeMultiplier;
            thisEnemy.moveSpeed = Random.Range(enemySpeed.x, enemySpeed.y);

            spawnTimer = 0f;
        }
    }

    public static void enemyDeath(GameObject enemy)
    {
        enemyList.Remove(enemy);
        GameManager.enemiesKilled += 1;
    }

    public static void enemyThrough(GameObject enemy)
    {
        enemyList.Remove(enemy);
        GameManager.enemiesThrough += 1;
    }
}
