﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour {

	void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.GetComponent<Enemy>())
        {
            EnemySpawner.enemyDeath(col.gameObject);
        }

        Destroy(col.gameObject);
    }
}
