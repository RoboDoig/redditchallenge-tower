﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TowerControl : MonoBehaviour {

    [Header ("Targeting")]
    public Transform target;
    public Transform shootPosition;
    public bool useSmartTargeting = true;

    [Header ("Projectile Settings")]
    public GameObject projectile;
    public float fireDelay; // How long to wait between shots 
    public float projectileSpeed;

    [Header("Test Settings")]
    public bool showShootDirection = false;
    public float shootDirectionTestLength = 20f; // How long our gizmos line is for testing shoot direction

    private Vector3 lastTargetPosition = Vector3.zero; // For tracking target and calculating velocity, initial guess at (0,0,0)
    private float projectileTimer = 0f;
    private Vector3 currentAimDirection;

    public static List<Transform> targetsList;

    void Update()
    {
        ProjectileTimer();

        if (target != null && target)
        {
            // Get the potential shoot direction to hit our target
            Vector3 targetVelocity = GetTargetVelocity();
            if (useSmartTargeting) {
                currentAimDirection = Targeting.CalculateAimDirection(shootPosition.position, target.position, targetVelocity, projectileSpeed);
            } else
            {
                currentAimDirection = Targeting.DumbAimDirection(shootPosition.position, target.position);
            }

            // And shoot if we're allowed to (by the timer)
            ProjectileShoot(currentAimDirection);
        } else
        {
            GetTarget();
        }
    }

    void GetTarget()
    {
        if (EnemySpawner.enemyList.Count>0)
        {
            target = EnemySpawner.enemyList[Mathf.RoundToInt(Random.Range(0, EnemySpawner.enemyList.Count))].transform;
        }
    }

    void ProjectileTimer()
    {
        projectileTimer += Time.deltaTime;
    }

    void ProjectileShoot(Vector3 aimDirection)
    {
        if (projectileTimer > fireDelay && target != null)
        {
            GameObject projectileInstance = (GameObject)Instantiate(projectile, shootPosition.position, Quaternion.identity);
            projectileInstance.GetComponent<Projectile>().shooter = this.transform;
            projectileInstance.GetComponent<Rigidbody>().velocity = aimDirection * projectileSpeed;
            projectileTimer = 0f;
        }
    }

    Vector3 GetTargetVelocity()
    {
        Vector3 targetVelocity = Targeting.CalculateTargetVelocity(lastTargetPosition, target.position, Time.deltaTime);
        lastTargetPosition = target.position;

        return targetVelocity;
    }

    void OnDrawGizmos()
    {
        if (showShootDirection)
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(shootPosition.position, shootPosition.position + currentAimDirection * shootDirectionTestLength);
        }
    }
}
