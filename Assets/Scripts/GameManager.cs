﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

    public static int enemiesThrough;
    public static int enemiesKilled;

    void Start()
    {
        enemiesThrough = 0;
        enemiesKilled = 0;
    }

    void OnGUI()
    {
        GUI.color = Color.black;
        GUI.Label(new Rect(10, 10, 500, 20), ("Enemies Through: " + enemiesThrough.ToString()));
        GUI.Label(new Rect(10, 40, 500, 20), ("Enemies Killed: " + enemiesKilled.ToString()));
    }
}
