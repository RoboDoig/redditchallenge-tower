﻿using UnityEngine;
using System.Collections;

public class EndZone : MonoBehaviour {

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.GetComponent<Enemy>())
        {
            EnemySpawner.enemyThrough(col.gameObject);
        }
        Destroy(col.gameObject);
    }
}
