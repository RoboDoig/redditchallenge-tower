﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

    public float damageBase;
    public Vector2 damageRange;
    public float maxTimeAlive;

    public Transform shooter;

    private Enemy thisEnemy;
    private float aliveTimer;

    void Start()
    {
        aliveTimer = 0f;
    }

    void Update()
    {
        aliveTimer += Time.deltaTime;
        if (aliveTimer > maxTimeAlive)
            Destroy(gameObject);
    }

    void OnCollisionEnter(Collision col)
    {
        thisEnemy = col.gameObject.GetComponent<Enemy>();
        if (thisEnemy != null)
        {
            thisEnemy.hp -= damageBase * Random.Range(damageRange.x,damageRange.y);
            thisEnemy.attacker = this.shooter;
        }
        Destroy(gameObject);
    }
}
