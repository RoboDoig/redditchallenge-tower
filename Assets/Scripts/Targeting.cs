﻿using UnityEngine;
using System.Collections;

public class Targeting : MonoBehaviour {

    public static Vector3 CalculateTargetVelocity(Vector3 targetLastPosition, Vector3 targetCurrentPosition, float deltaTime)
    {
        return (targetCurrentPosition - targetLastPosition) / deltaTime;
    }

    public static Vector3 DumbAimDirection(Vector3 shootPosition, Vector3 targetPosition)
    {
        return (targetPosition - shootPosition).normalized;
    }

    // Note: does not take gravity into account - rigidbody projectile must not have gravity applied for successful hit
    public static Vector3 CalculateAimDirection(Vector3 shootPosition, Vector3 targetPosition, Vector3 targetVelocity, float projectileSpeed)
    {
        float t = Mathf.Infinity; // default t value in the case that we can't solve for t later
        Vector3 vToTarget = targetPosition - shootPosition; // Vector pointing from where we want to shoot to our target's position

        // solving quadratic equation for t
        float a = (projectileSpeed * projectileSpeed) - (targetVelocity.sqrMagnitude);
        float b = 2 * vToTarget.magnitude * targetVelocity.magnitude * Vector3.Dot(vToTarget.normalized, targetVelocity.normalized);
        float c = -(vToTarget.sqrMagnitude);

        float t1 = (-b + Mathf.Sqrt((b * b) - 4f * a * c)) / (2f * a);
        float t2 = (-b - Mathf.Sqrt((b * b) - 4f * a * c)) / (2f * a);

        // getting the smallest non-negative value of t
        t = Mathf.Min(t1, t2);
        if (t < 0)
        {
            t = Mathf.Max(t1, t2);
        }

        Vector3 projectileVelocityVec = targetVelocity + (vToTarget / t);
        Vector3 aimDirection = projectileVelocityVec.normalized;

        // returns a normalised direction to fire in which we can multiply by our projectile speed to (hopefully) make a collision course with the target
        return aimDirection;
    }
}
